<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
    }
    
    function get_imgCategoria($idcategoria)
    {
        $img = APPPATH.'files/img/categorias/'.$idcategoria; // ADAPTAÇÃO TEMPORÁRIA

		if(!is_file($img)) $img = APPPATH.'files/img/categorias/default.png';
		
		
		require(APPPATH.'classes/wideimage/WideImage.php');
		$image = wideImage::load($img);
		$tamanho = ($image->getWidth() > $image->getHeight()) ? $image->getHeight() : $image->getWidth();
		$tamanho = ($tamanho > 400) ? 400 : $tamanho;
		$image = $image->resize(400, 300);
		//$image = $image->crop("center" ,"center" , 400, 300);
		$image->output("jpg", 80);
    }
    
    function get_imgReceita($idreceita)
    {
        $image = APPPATH.'files/img/receitas/'.$idreceita; // ADAPTAÇÃO TEMPORÁRIA

        if(!is_file($image)) $image = APPPATH.'files/img/receitas/default.png';
		
		$type = pathinfo($image, PATHINFO_EXTENSION);
		header('Content-type:image/'.$type);

		readfile($image);
    }
    
    function get_thumbReceita($idreceita)
    {
        $img = APPPATH.'files/img/receitas/'.$idreceita; // ADAPTAÇÃO TEMPORÁRIA

        if(!is_file($img)) $img = APPPATH.'files/img/receitas/default.png';
		
		require(APPPATH.'classes/wideimage/WideImage.php');
		$image = wideImage::load($img);
		$tamanho = ($image->getWidth() > $image->getHeight()) ? $image->getHeight() : $image->getWidth();
		$tamanho = ($tamanho > 400) ? 400 : $tamanho;
		$image = $image->crop("center" ,"center" , $tamanho, $tamanho);
		$image = $image->resize(80, 80);
		$image->output("jpg", 80);
    }
}
