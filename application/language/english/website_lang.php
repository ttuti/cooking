<?php

$lang['menu_home'] = 'Home';
$lang['menu_find'] = 'Find a recipe';
$lang['menu_category'] = 'Categories';
$lang['menu_foodbook'] = 'Recipe\'s Book';
$lang['menu_recipes'] = 'Recipes';
$lang['menu_order'] = 'Order';
$lang['menu_contact'] = 'Contacts';


//==========================================

$lang['category'] = 'Category';
$lang['categories'] = 'Categories';
$lang['recipes'] = 'Recipes';
$lang['view_more'] = 'View more';
$lang['views'] = 'Views';
$lang['item'] = 'Item';
$lang['itens'] = 'Itens';


//PAISES
$lang['angola'] = 'Angola';
$lang['portugal'] = 'Portugal';
$lang['usa'] = 'United States of America';

?>