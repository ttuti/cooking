<?php

$lang['menu_home'] = 'Página Inicial';
$lang['menu_find'] = 'Encontre uma receita';
$lang['menu_category'] = 'Categorias';
$lang['menu_foodbook'] = 'Livro de receitas';
$lang['menu_recipes'] = 'Receitas';
$lang['menu_order'] = 'Encomendar';
$lang['menu_contact'] = 'Contactos';


//==========================================

$lang['category'] = 'Categoria';
$lang['categories'] = 'Categorias';
$lang['recipes'] = 'Receitas';
$lang['view_more'] = 'Ver mais';
$lang['views'] = 'Visualizações';
$lang['item'] = 'Item';
$lang['itens'] = 'Itens';


//PAISES
$lang['angola'] = 'Angola';
$lang['portugal'] = 'Portugal';
$lang['usa'] = 'Estados Unidos da America';

?>