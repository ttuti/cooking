<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mdl_geral extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_receitas($filters = '')
	{
		$strQuery = "SELECT * FROM comidas";
		$query = $this->db->query($strQuery);
		$result = $query->result_array();
		
		return $result;
	}

	public function get_receitaById($id)
	{
		$strQuery = "SELECT * FROM comidas WHERE comida_id = '$id'";
		$query = $this->db->query($strQuery);
		$result = $query->row_array();
		
		return $result;
	}

	public function get_categorias()
	{
		$strQuery = "SELECT * FROM categorias";
		$query = $this->db->query($strQuery);
		$result = $query->result_array();
		
		return $result;
	}
	
	/*
	public function get_userdata($username)
	{
		$strQuery = "SELECT * FROM colaboradores WHERE clb_username = '$username' AND clb_delReg = '0'";
		$query = $this->db->query($strQuery);
		$result = $query->row_array();
		
		return $result;
	}
	
	public function get_listUsers()
	{
		$strQuery = "SELECT * FROM colaboradores WHERE clb_delReg = '0'";
		$query = $this->db->query($strQuery);
		$result = $query->result_array();
		
		return $result;
	}
	
	public function get_userById($id)
	{
		$strQuery = "SELECT * FROM colaboradores WHERE clb_id = '".$id."'";
		$query = $this->db->query($strQuery);
		$result = $query->row_array();
		
		return $result;
	}
	*/
}
