<?php

$footer_categorias = lista("categorias");

?>

<footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4 ">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="<?php echo base_url(); ?>assets/#">
                                    <img src="<?php echo base_url(); ?>assets/img/footer_logo.png" alt="">
                                </a>
                            </div>
                            <p><?php echo $this->config->item("website_endereco"); ?> <br> 
                            <?php echo $this->config->item("website_cidade"); ?> - <?php echo $this->config->item("website_pais"); ?> <br>
                                <a href="call:93233"><?php echo $this->config->item("website_ntel")[0]; ?></a> <br>
                                <a href="mailto:contact@carpenter.com"><?php echo $this->config->item("website_email"); ?></a>
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url(); ?>#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>#">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Acompanhe
                            </h3>
                            <ul class="links">
                                <li><a href="<?php echo base_url(); ?>#">Prato do dia</a></li>
                                <li><a href="<?php echo base_url(); ?>#"> Encomendar</a></li>
                                <li><a href="<?php echo base_url(); ?>#">Concursos</a></li>
                                <li><a href="<?php echo base_url(); ?>#"> Contactos</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                <?php echo $this->lang->line("categories"); ?>
                            </h3>
                            <ul class="links double_links">

                                <?php
                                $_iCount = 1;
                                foreach ($footer_categorias as $key => $value)
                                {
                                    if($_iCount > 8) break;
                                    echo '<li><a href="'.base_url().'">'.$value.'</a></li>';
                                    $_iCount++;
                                }

                                ?>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Instagram
                            </h3>
                            <div class="instagram_feed">
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/1.png" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/2.png" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/3.png" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/4.png" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/5.png" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="<?php echo base_url(); ?>#">
                                        <img src="<?php echo base_url(); ?>assets/img/instagram/6.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;2020 Todos os direitos reservados | Este website foi desenvolvido <i class="fa fa-heart-o" aria-hidden="true"></i> por <a href="<?php echo base_url(); ?>#" target="_blank">ttuti</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


  <!-- Modal -->
  <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="serch_form">
            <input type="text" placeholder="Search" >
            <button type="submit">search</button>
        </div>
      </div>
    </div>
  </div>
    <!-- link that opens popup -->
<!--     
    <script src="<?php echo base_url(); ?>assets/https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/https://static.codepen.io/assets/common/stopExecutionOnTimeout-de7e2ef6bfefd24b79a3f68b414b87b8db5b08439cac3f1012092b2290c719cd.js"></script>

    <script src="<?php echo base_url(); ?>assets/ https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"> </script> -->
    <!-- JS here -->
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ajax-form.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scrollIt.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nice-select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>
   

    
    <!--contact js-->
    <script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }
        });
    </script>
</body>

</html>