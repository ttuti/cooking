<?php

$a_feedbacks = lista("top5feedbacks");

?>

<!-- testimonial_area  -->
    <div class="testimonial_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="testmonial_active owl-carousel">
                        
                        <?php

                        foreach ($a_feedbacks as $key => $value)
                        {
                            echo '<div class="single_carousel">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <div class="single_testmonial text-center">
                                                <div class="author_thumb">
                                                    <img src="'.base_url().'assets/img/testmonial/author.png" alt="">
                                                </div>
                                                <p>"'.$value["feedback_mensagem"].'</p>
                                                <div class="testmonial_author">
                                                    <h3>- '.$value["feedback_nome"].'</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                        }

                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /testimonial_area  -->