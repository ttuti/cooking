<?php

$a_receitas = lista("top10receitas");
$a_fTags = lista("food_tags");

?>


<div class="popular_places_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3><?php echo $this->lang->line("recipes"); ?></h3>
                        <p>Navegue na nossa coleção de Receitas e aprenda a confecionar os alimentos mais saborosos.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <?php
                
                foreach ($a_receitas as $key => $value)
                {
                    $vTags = json_decode($value["comida_tags"], TRUE);
                    $vTags = is_array($vTags) ? $vTags : array();

                    $selTag = empty($vTags) ? "-" : $vTags;

                    $hasTag = false;

                    $nomeTag = "-";
                    $corTag = "#fbfbfb";

                    foreach ($vTags as $_k => $_v)
                    {
                        if(isset($a_fTags[$_v]))
                        {
                            $hasTag = true;
                            $nomeTag = $a_fTags[$_v]["tag_nome"];
                            $corTag = $a_fTags[$_v]["tag_color"];
                            break;
                        }
                    }
                    

                    echo '<div class="col-lg-4 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="'.base_url().'File/get_imgReceita/'.md5($key).'" alt="">
                                    <a href="'.base_url().'#" class="prise prise-fix"><i>'.$nomeTag.'</i></a>
                                </div>
                                <div class="place_info">
                                    <a href="'.base_url().'#"><h3>'.$value["comida_nome"].'</h3></a>
                                    <p>'.$this->lang->line(getCountryByCode($value["comida_pais"])).'</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i>
                                            <a href="'.base_url().'#">(20 '.$this->lang->line("views").')</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="'.base_url().'#">5 Days</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }

                ?>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="more_place_btn text-center">
                        <a class="boxed-btn4" href="<?php echo base_url(); ?>#"><?php echo $this->lang->line("view_more"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>