<?php

$a_countDadosTipo = lista("countItemsByCategory");
$a_categorias = lista("categorias");
?>

<!-- popular_destination_area_start  -->
    <div class="popular_destination_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3><?php echo $this->lang->line("categories"); ?></h3>
                        <p>Navegue entre as diversas categorias disponíveis em nosso portal.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <?php

                foreach ($a_countDadosTipo as $key => $value)
                {
                    if(!isset($a_categorias[$key])) continue;

                    $_nomeCateg = $a_categorias[$key];
                    echo '<div class="col-lg-4 col-md-6">
                            <div class="single_destination">
                                <div class="thumb">
                                    <img src="'.base_url().'File/get_imgCategoria/'.md5($key).'" alt="">
                                </div>
                                <div class="content">
                                    <p class="d-flex align-items-center">'.$_nomeCateg.' <a href="'.base_url().'Receitas/ver/'.$key.'">  '.$value.' '.$this->lang->line(get_keyLangByCount("item", $value)).'</a> </p>
                                    
                                </div>
                            </div>
                        </div>';
                }

                ?>

                
            </div>
        </div>
    </div>
    <!-- popular_destination_area_end  -->