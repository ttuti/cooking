<nav>
    <ul id="navigation">
        <li><a class="active" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("menu_home"); ?></a></li>
        <li><a href="<?php echo base_url(); ?>Categorias"><?php echo $this->lang->line("menu_category"); ?></a></li>
        <li><a href="<?php echo base_url(); ?>#"><?php echo $this->lang->line("menu_recipes"); ?> <i class="ti-angle-down"></i></a>
            <ul class="submenu">
                    <li><a href="<?php echo base_url(); ?>Receitas"><?php echo $this->lang->line("menu_find"); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>Receitas/livro"><?php echo $this->lang->line("menu_foodbook"); ?></a></li>
            </ul>
        </li>
        <li><a class="" href="<?php echo base_url(); ?>assets/travel_destination.html"><?php echo $this->lang->line("menu_order"); ?></a></li>
        <li><a href="<?php echo base_url(); ?>assets/#">blog <i class="ti-angle-down"></i></a>
            <ul class="submenu">
                <li><a href="<?php echo base_url(); ?>assets/blog.html">blog</a></li>
                <li><a href="<?php echo base_url(); ?>assets/single-blog.html">single-blog</a></li>
            </ul>
        </li>
        <li><a href="<?php echo base_url(); ?>Contactos"><?php echo $this->lang->line("menu_contact"); ?></a></li>
    </ul>
</nav>