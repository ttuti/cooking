<?php

$a_countDadosTipo = lista("countItemsByCategory");
$a_categorias = lista("categorias");
$a_fTags = lista("food_tags");
$a_recomendReceitas = lista("random5receitas");

?>

<div class="bradcam_area bradcam_bg_2">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h3>Receitas</h3>
                    <p>Pixel perfect design with awesome contents</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="where_togo_area where_togo_area-fix">
    <div class="container">
        sds
    </div>
</div>





<section class="popular_places_area section-padding">
    <div class="container">
        <div class="row">


            









        <div class="col-lg-8">
            <div class="row">
                
                <?php
                
                foreach ($dados as $key => $value)
                {
                    $vTags = json_decode($value["comida_tags"], TRUE);
                    $vTags = is_array($vTags) ? $vTags : array();

                    $selTag = empty($vTags) ? "-" : $vTags;

                    $hasTag = false;

                    $nomeTag = "-";
                    $corTag = "#fbfbfb";

                    foreach ($vTags as $_k => $_v)
                    {
                        if(isset($a_fTags[$_v]))
                        {
                            $hasTag = true;
                            $nomeTag = $a_fTags[$_v]["tag_nome"];
                            $corTag = $a_fTags[$_v]["tag_color"];
                            break;
                        }
                    }
                    

                    echo '<div class="col-lg-6 col-md-6">
                            <div class="single_place">
                                <div class="thumb">
                                    <img src="'.base_url().'File/get_imgReceita/'.md5($key).'" alt="">
                                    <a href="'.base_url().'#" class="prise prise-fix"><i>'.$nomeTag.'</i></a>
                                </div>
                                <div class="place_info">
                                    <a href="'.base_url().'Receitas/ver/'.$value["comida_id"].'"><h3>'.$value["comida_nome"].'</h3></a>
                                    <p>'.$this->lang->line(getCountryByCode($value["comida_pais"])).'</p>
                                    <div class="rating_days d-flex justify-content-between">
                                        <span class="d-flex justify-content-center align-items-center">
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i> 
                                            <i class="fa fa-star"></i>
                                            <a href="'.base_url().'#">(20 '.$this->lang->line("views").')</a>
                                        </span>
                                        <div class="days">
                                            <i class="fa fa-clock-o"></i>
                                            <a href="'.base_url().'#">5 Days</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
                }

                ?>

                
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="more_place_btn text-center">


                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Previous">
                                        <i class="ti-angle-left"></i>
                                    </a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link">1</a>
                                </li>
                                <li class="page-item active">
                                    <a href="#" class="page-link">2</a>
                                </li>
                                <li class="page-item">
                                    <a href="#" class="page-link" aria-label="Next">
                                        <i class="ti-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
            
                        
                    </div>
                </div>
            </div>
        </div>
















        
            
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <form action="#">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder="Pesquisar" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pesquisar'">
                                    <div class="input-group-append">
                                        <button class="btn" type="button"><i class="ti-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Pesquisar</button>
                        </form>
                    </aside>

                    <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title"><?php echo $this->lang->line("categories"); ?></h4>
                        <ul class="list cat-list">
                            
                            <?php

                            foreach ($a_countDadosTipo as $key => $value)
                            {
                                if(!isset($a_categorias[$key])) continue;

                                echo '<li>
                                        <a href="#" class="d-flex">
                                            <p>'.($a_categorias[$key]).'</p>
                                            <p>&nbsp;&nbsp;('.$value.')</p>
                                        </a>
                                    </li>';
                            }

                            ?>
                            
                        </ul>
                    </aside>

                    <aside class="single_sidebar_widget popular_post_widget">
                        <h3 class="widget_title">Veja Também</h3>
                        
                        <?php

                        foreach ($a_recomendReceitas as $key => $value)
                        {
                            echo '<div class="media post_item">
                                    <img src="'.base_url().'File/get_thumbReceita/'.md5($value["comida_id"]).'" alt="post">
                                    <div class="media-body">
                                        <a href="#">
                                            <h3>'.$value["comida_nome"].'</h3>
                                        </a>
                                        <p>'.date("Y-m-d", strtotime($value["comida_dtCria"])).'</p>
                                    </div>
                                </div>';
                        }

                        ?>

                    </aside>
                    <aside class="single_sidebar_widget tag_cloud_widget">
                        <h4 class="widget_title">Tags</h4>
                        <ul class="list">
                            
                            <?php
                            foreach ($a_fTags as $key => $value)
                            {
                                echo '<li>
                                        <a href="#">'.$value["tag_nome"].'</a>
                                    </li>';
                            }
                            ?>
                            
                        </ul>
                    </aside>


                </div>
            </div>
        </div>
    </div>
</section>