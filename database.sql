-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: cooking
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `categ_id` int(11) NOT NULL AUTO_INCREMENT,
  `categ_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `categ_img` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `categ_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `categ_userCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`categ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Bolos',NULL,NULL,NULL),(2,'Salgados',NULL,NULL,NULL),(3,'Sopas',NULL,NULL,NULL),(4,'Gelados',NULL,NULL,NULL),(5,'Sucos',NULL,NULL,NULL),(6,'Chás',NULL,NULL,NULL),(7,'Bolinhos',NULL,NULL,NULL);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comidas`
--

DROP TABLE IF EXISTS `comidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comidas` (
  `comida_id` int(11) NOT NULL AUTO_INCREMENT,
  `comida_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_ingred` text COLLATE utf8_bin,
  `comida_preparo` text COLLATE utf8_bin,
  `comida_finalizar` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_acomp` text COLLATE utf8_bin,
  `comida_npessoas` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_userCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_pais` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_historia` text COLLATE utf8_bin,
  `comida_recomend` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_inventor` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_categoria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `comida_tags` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`comida_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comidas`
--

LOCK TABLES `comidas` WRITE;
/*!40000 ALTER TABLE `comidas` DISABLE KEYS */;
INSERT INTO `comidas` VALUES (1,'teste 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ao',NULL,NULL,NULL,'1','[1,3,5]'),(2,'segundo test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ao',NULL,NULL,NULL,'2','[2,3]'),(3,'Nada disso',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ao',NULL,NULL,NULL,'2','[2,3]');
/*!40000 ALTER TABLE `comidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `empresa_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_ntel` text COLLATE utf8_bin,
  `empresa_facebook` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `empresa_instagram` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `empresa_pinterest` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `empresa_twitter` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `empresa_youtube` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `empresa_endereco` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_cidade` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_pais` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_logotipo` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `empresa_responsavel` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedbacks`
--

DROP TABLE IF EXISTS `feedbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedbacks` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `feedback_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `feedback_email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `feedback_user` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `feedback_data` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `feedback_mensagem` text COLLATE utf8_bin,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedbacks`
--

LOCK TABLES `feedbacks` WRITE;
/*!40000 ALTER TABLE `feedbacks` DISABLE KEYS */;
INSERT INTO `feedbacks` VALUES (1,'Micky Mouse',NULL,NULL,'2020-03-12 16:23:15','Working in conjunction with humanitarian aid agencies, we have supported programmes to help alleviate human suffering.'),(2,'Micky Mouse',NULL,NULL,'2020-03-12 16:23:15','Working in conjunction with humanitarian aid agencies, we have supported programmes to help alleviate human suffering.'),(3,'Micky Mouse',NULL,NULL,'2020-03-12 16:23:15','Working in conjunction with humanitarian aid agencies, we have supported programmes to help alleviate human suffering.');
/*!40000 ALTER TABLE `feedbacks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_nome` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tag_dtCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tag_userCria` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Delicioso',NULL,NULL),(2,'Saboroso',NULL,NULL),(3,'Nutritivo',NULL,NULL),(4,'Saudável',NULL,NULL);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-19 18:58:06
